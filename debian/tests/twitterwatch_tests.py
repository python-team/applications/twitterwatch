#!/usr/bin/env python3

import pytest
from twitterwatch import main
import sys
import os

# pass fake conf file to argv
conf_file_path = os.path.join(os.path.dirname(__file__), 'twitter_ini.txt')
sys.argv.append(conf_file_path)


def test_config_reader():
    conf = main.ConfParse(sys.argv[-1])
    assert 'ml9jaiBnf3pmU9uIrKNIxAr3v' == conf.consumer_key
    assert 'foo@mylaptop.org' == conf.mailto


def test_using_dummy_keys_should_return_unauthorized():
    with pytest.raises(Exception) as e_info:
        main.Main()
    assert 'Unauthorized' == e_info.typename


if __name__ == '__main__':
    pytest.main([__file__])
